const mongoose = require('mongoose')

const Gastos = mongoose.model('Gastos', {
    date: String,
    title: String,
    valorFormat: Number,
    banho: Boolean,
    vet: Boolean,
    comida: Boolean,
})

module.exports = Gastos