require('dotenv').config()
const express = require('express');
const mongoose = require('mongoose');
var cors = require('cors');//cors
const app = express();

app.use(cors());//cors
app.use(
    express.urlencoded({
        extended: true
    })
)

app.use(express.json())

const personRoutes = require('./routes/personRoutes')
const gastoRoutes = require('./routes/gastoRoutes')
const gastosRoutes = require('./routes/gastosRoutes')

app.use('/person', personRoutes)
app.use('/gasto', gastoRoutes)
app.use('/gastos', gastosRoutes)

app.get('/', (req,res)=>{
    res.json({message: 'Oi Express'})
})
const DB_USER = process.env.DB_USER
const DB_PASSWORD = encodeURIComponent(process.env.DB_PASSWORD)

mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASSWORD}@zeuscluster.tibv4g1.mongodb.net/projectzeusdb?retryWrites=true&w=majority`).then(
    ()=>{
        console.log("Conectado ao MongoDB")
        app.listen(5001)
    }
).catch((err)=>console.log(err)

)

//app.listen(3000)

